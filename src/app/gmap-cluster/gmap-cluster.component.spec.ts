import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GmapClusterComponent } from './gmap-cluster.component';

describe('GmapClusterComponent', () => {
  let component: GmapClusterComponent;
  let fixture: ComponentFixture<GmapClusterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GmapClusterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GmapClusterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
