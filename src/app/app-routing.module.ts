import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GmapClusterComponent } from './gmap-cluster/gmap-cluster.component';

const routes: Routes = [
  {
    path: '',
    component: GmapClusterComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
